#from bessyii.eLog import requestInvestigationName,getInvestigationIDFromTitle,getInvestigationIDFromName,getSessionID, writeToELog
from bessyii.eLog import requestInvestigationName,getSessionID, writeToELog, ELogCallback, authenticate_session, logout_session
from bessyii.locks import teardown_my_shell,lock_resource
from getpass import getpass
import Levenshtein as lev
import requests
import logging
import json
from datetime import datetime, timedelta
from bluesky.callbacks import CallbackBase
from pprint import pformat
import atexit
from .base import *
from .beamline import *

import socket
RE.md['hostname'] = socket.gethostname()



#######   ---- eLog ----------------------


from jinja2 import Template
start_template ="""
<b> Plan Started by {{user_name}}</b>
<br>{{- plan_name }} ['{{ uid[:6] }}'] (scan num: {{ scan_id }})
<br> command: {{ command_elog }}
<br>---------
{% if  operator is defined %}
    <br>operator :    {{operator}}
{% endif %}
{% if  reason is defined %}
    <br>reason :    {{reason}}
{% endif %}
{% if  comment is defined %}
    <br>comment :    {{comment}}
{% endif %}
<hr style="height:2px;border:none;color:#333;background-color:#333;" />"""



j2_start_template = Template(start_template)


end_template ="""
<hr style="height:2px;border:none;color:#333;background-color:#333;" />
<b>Plan ended</b>
<br>exit_status: {{exit_status}}
<br>num_events: {{num_events}}
<br>uid:{{run_start}}
<br>
"""


j2_end_template = Template(end_template)

# modifiy the template according to beamline needs
# only  one example relative to a pgm is left here as an example
beamline_status_template ="""
<b>Beamline Status</b>
<br>pgm :    {{pgm_en}}
"""

j2_baseline_template = Template(beamline_status_template)

# uncomment once lock is installed

# Set up our locks and environment
lock_list = [lock]

def logout():
    logout_session(RE,lock_list)

def authenticate():
    authenticate_session(RE,db,lock_list)
    
## Connect the shell teardown on exit
atexit.register(teardown_my_shell,RE,lock_list)  

## Find out what the user wants to do
choice = input(f"\nConnect to eLog? Y/N:")


#Clear the metadata
if 'investigation_title' in RE.md:
    del RE.md['investigation_title']

if 'investigation_id' in RE.md:
    del RE.md['investigation_id']

if 'eLog_id_num' in RE.md:
    del RE.md['eLog_id_num']

if 'user_profile' in RE.md:
    del RE.md['user_profile']

if 'user_name' in RE.md:
    del RE.md['user_name']

if choice == 'Y' or choice == 'y':

    authenticate()
    

else: 
    
    print("You can always authenticate later with \'authenticate()\'")
    lock_resource(RE,lock_list)

#Subscribe the callback
RE.subscribe(ELogCallback(db,j2_start_template,j2_baseline_template,j2_end_template))


