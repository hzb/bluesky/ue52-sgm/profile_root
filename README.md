**IPython Profile control of BEAMLINE_NAME**

Three packages are used

| package | description |
| ------ | ------ |
|[bessyii](https://gitlab.helmholtz-berlin.de/bessyII/bluesky/bessyii) | contains shared scripts, plans and tools for the whole facility |
|[bessyii_devices](https://gitlab.helmholtz-berlin.de/bessyII/bluesky/bessyii_devices) | contains ophyd devices for bessyii |
|[BEAMLINE_NAMEtools](add url) | contains plans, scripts and tools specific to this beamline |

Make sure that you update setuptools, and pip and have wheel installed
